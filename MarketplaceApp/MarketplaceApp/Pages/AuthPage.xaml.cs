﻿using MarketplaceApp.Pages.AdminPages;
using MarketplaceApp.Pages.ClientPages;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace MarketplaceApp.Pages
{
    public partial class AuthPage : Page
    {
        public AuthPage()
        {
            InitializeComponent();
        }

        private void btnAuth_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(tbLogin.Text) && !String.IsNullOrEmpty(pbPassword.Password))
            {
                var user = App.Connection.User.FirstOrDefault(x => x.Login == tbLogin.Text && x.Password == pbPassword.Password);

                if (user != null)
                {
                    App.currentUser = user;

                    switch (user.Role.Title) 
                    {
                        case "Администратор":
                            NavigationService.Navigate(new MainAdminPage());
                            break;
                        case "Пользователь":
                            NavigationService.Navigate(new MainClientPage());
                            break;
                        default:
                            ShowErrorMessageBox("Произошла неизвестная ошибка");
                            break;
                    }
                }
                else
                {
                    ShowErrorMessageBox("Неверный логин или пароль");
                }
            }

            else
            {
                ShowErrorMessageBox("Заполните все поля");
            }
        }

        private void btnGoToRegistrationPage_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new RegistrationPage());
        }

        private void ShowErrorMessageBox(String text)
        {
            MessageBox.Show(text, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
