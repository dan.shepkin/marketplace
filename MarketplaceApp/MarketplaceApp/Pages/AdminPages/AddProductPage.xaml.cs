﻿using MarketplaceApp.ADOApp;
using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MarketplaceApp.Pages.AdminPages
{
    public partial class AddProductPage : Page
    {
        public AddProductPage()
        {
            InitializeComponent();
            FillType();
        }

        public byte[] ProductImage;

        private void FillType() 
        {
            cbType.Items.Clear();
            cbType.ItemsSource = App.Connection.ProductType.ToList();
        }

        private void btnTakeImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                if (dialog.ShowDialog() != null)
                {
                    ProductImage = File.ReadAllBytes(dialog.FileName);

                    imgProduct.Source = (new ImageSourceConverter()).ConvertFromString(dialog.FileName) as ImageSource;
                }
            }
            catch
            {
                ShowErrorMessageBox("Выберите изображение");
            }
        }

        private void btnAddProduct_Click(object sender, RoutedEventArgs e)
        {
            if (tbName.Text != String.Empty && tbArticle.Text != String.Empty && tbCost.Text != String.Empty &&
                tbDescription.Text != String.Empty && cbType.SelectedItem != null && imgProduct.Source != null)
            {
                App.Connection.Product.Add(new Product()
                {
                    Name = tbName.Text,
                    Cost = Convert.ToInt32(tbCost.Text),
                    Description = tbDescription.Text,
                    Article = tbArticle.Text,
                    Image = ProductImage,
                    ProductType_ID = (cbType.SelectedItem as ProductType).ProductType_ID,
                });

                App.Connection.SaveChanges();

                MessageBox.Show("Товар успешно добавлен", "Статус", MessageBoxButton.OK, MessageBoxImage.None);

                ClearAllFields();
            }

            else
            {
                ShowErrorMessageBox("Заполните все поля");
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new MainAdminPage());
        }

        private void ShowErrorMessageBox(String text)
        {
            MessageBox.Show(text, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void ClearAllFields()
        {
            tbName.Text = "";
            tbArticle.Text = "";
            tbCost.Text = "";
            tbDescription.Text = "";
            cbType.SelectedItem = null;
            imgProduct.Source = null;
        }
    }
}
