﻿using MarketplaceApp.ADOApp;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace MarketplaceApp.Pages.AdminPages
{
    public partial class MainAdminPage : Page
    {
        public MainAdminPage()
        {
            InitializeComponent();
            CBType.Items.Clear();
            var types = App.Connection.ProductType.ToList();
            types.Insert(0, new ProductType() { Title = "Показать всё" });
            CBType.ItemsSource = types.ToList();
            CBType.SelectedIndex = 0;
            FillProducts();
        }

        private void FillProducts() 
        {
            lvProducts.Items.Refresh();
            
            var filtred = App.Connection.Product.ToList();
            var surchText = TBSurch.Text.ToLower();
            var type = CBType.SelectedItem as ProductType;
            if (!string.IsNullOrWhiteSpace(TBSurch.Text))
                filtred = filtred.Where(x => x.Name.ToLower().Contains(surchText)).ToList();
            if(type != null && CBType.SelectedIndex != 0)
                filtred = filtred.Where(x => x.ProductType_ID == type.ProductType_ID).ToList();
            lvProducts.ItemsSource = filtred.ToList();
        }

        private void btnRemoveProduct_Click(object sender, RoutedEventArgs e)
        {
            App.Connection.Product.Remove((sender as Button).DataContext as Product);
            App.Connection.SaveChanges();
            FillProducts();
            MessageBox.Show("Товар успешно удален", "Успех", MessageBoxButton.OK, MessageBoxImage.None);
        }

        private void btnGoAddProduct_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AddProductPage());
        }

        private void TBSurch_TextChanged(object sender, TextChangedEventArgs e)
        {
            FillProducts();

        }

        private void CBType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FillProducts();

        }
    }
}
