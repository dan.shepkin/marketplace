﻿using MarketplaceApp.ADOApp;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace MarketplaceApp.Pages
{
    public partial class RegistrationPage : Page
    {
        public RegistrationPage()
        {
            InitializeComponent();
        }

        private void btnRegistration_Click(object sender, RoutedEventArgs e)
        {
            if (tbSurname.Text != "" && tbName.Text != "" && tbPatronymic.Text != "" 
                && tbLogin.Text != "" && pbPassword.Password != "")
            {
                User user = App.Connection.User.FirstOrDefault(x => x.Login == tbLogin.Text);

                if (user != null)
                {
                    ShowErrorMessageBox("Пользователь с таким логином уже существует");
                }

                else
                {
                    User newUser = new User()
                    {
                        Surname = tbSurname.Text,
                        Name = tbName.Text,
                        Patronymic = tbPatronymic.Text,
                        Login = tbLogin.Text,
                        Password = pbPassword.Password,
                        Role_ID = 2
                    };

                    App.Connection.User.Add(newUser);
                    App.Connection.SaveChanges();
                    ShowErrorMessageBox("Вы успешно зарегистрировались");
                    NavigationService.Navigate(new AuthPage());
                }
            }
            else
            {
                ShowErrorMessageBox("Заполните все поля");
            }
        }

        private void btnAuth_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AuthPage());
        }

        private void ShowErrorMessageBox(String text)
        {
            MessageBox.Show(text, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
