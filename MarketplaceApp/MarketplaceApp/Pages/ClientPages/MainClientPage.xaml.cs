﻿using MarketplaceApp.ADOApp;
using MarketplaceApp.Pages.AdminPages;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace MarketplaceApp.Pages.ClientPages
{
    public partial class MainClientPage : Page
    {
        public MainClientPage()
        {
            InitializeComponent();
            CBType.Items.Clear();
            var types = App.Connection.ProductType.ToList();
            types.Insert(0, new ProductType() { Title = "Показать всё" });
            CBType.ItemsSource = types.ToList();
            CBType.SelectedIndex = 0;
            FillProducts();
        }

        private void FillProducts()
        {
            lvProducts.Items.Refresh();

            var filtred = App.Connection.Product.ToList();
            var surchText = TBSurch.Text.ToLower();
            var type = CBType.SelectedItem as ProductType;
            if (!string.IsNullOrWhiteSpace(TBSurch.Text))
                filtred = filtred.Where(x => x.Name.ToLower().Contains(surchText)).ToList();
            if (type != null && CBType.SelectedIndex != 0)
                filtred = filtred.Where(x => x.ProductType_ID == type.ProductType_ID).ToList();
            lvProducts.ItemsSource = filtred.ToList();
        }

        private void btnAddToBasket_Click(object sender, RoutedEventArgs e)
        {
            App.basket.Add((sender as Button).DataContext as Product);
            MessageBox.Show("Товар успешно добавлен в корзину", "Успех", MessageBoxButton.OK, MessageBoxImage.None);
        }

        private void btnGoToBasket_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new BasketPage());
        }

        private void TBSurch_TextChanged(object sender, TextChangedEventArgs e)
        {
            FillProducts();

        }

        private void CBType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FillProducts();

        }
    }
}
