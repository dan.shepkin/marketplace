﻿using MarketplaceApp.ADOApp;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace MarketplaceApp.Pages.ClientPages
{
    public partial class BasketPage : Page
    {
        public BasketPage()
        {
            InitializeComponent();
            FillProducts();
        }

        private void FillProducts()
        {
            lvProducts.Items.Refresh();
            lvProducts.ItemsSource = App.basket;
        }

        private void btnRemoveFromBasket_Click(object sender, RoutedEventArgs e)
        {
            App.basket.Remove((sender as Button).DataContext as Product);
            FillProducts();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new MainClientPage());
        }
    }
}
