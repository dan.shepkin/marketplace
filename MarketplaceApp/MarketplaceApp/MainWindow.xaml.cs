﻿using MarketplaceApp.Pages;
using System.Windows;

namespace MarketplaceApp
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Navigate(new AuthPage());
        }
    }
}
