﻿using MarketplaceApp.ADOApp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace MarketplaceApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static MarketPlaceEntities Connection = new MarketPlaceEntities();
        public static User currentUser;
        public static List<Product> basket = new List<Product>();
    }
}
